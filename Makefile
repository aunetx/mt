# Could be for example: "/usr/local", "$(HOME)/.local"...
INSTALL_DIR = /usr/local
ROOT_PREFIX =

# Hacky way to check if we need root privileges to install
ifneq (,$(INSTALL_DIR) == *$(HOME)*)
ROOT_PREFIX = sudo
endif

# Just tell make that clean, install, and uninstall doesn't generate files
.PHONY: clean install uninstall

# # Build the release of the application
build-release: 
	cargo build --release

build-devel:
	cargo build

# Install the application
install: build-release
	$(ROOT_PREFIX) mkdir -p $(INSTALL_DIR)/bin
	$(ROOT_PREFIX) mkdir -p $(INSTALL_DIR)/share/Mt/profiles
	$(ROOT_PREFIX) mkdir -p $(INSTALL_DIR)/share/applications
	$(ROOT_PREFIX) mkdir -p $(INSTALL_DIR)/share/icons/hicolor/scalable/apps

	$(ROOT_PREFIX) install -m 755 target/release/mt $(INSTALL_DIR)/bin/Mt
	$(ROOT_PREFIX) install -m 644 data/profile/Default.ron $(INSTALL_DIR)/share/Mt/profiles/Default.ron
	$(ROOT_PREFIX) install -m 644 data/com.gitlab.miridyan.Mt.desktop $(INSTALL_DIR)/share/applications/com.gitlab.miridyan.Mt.desktop
	$(ROOT_PREFIX) install -m 644 data/icons/com.gitlab.miridyan.Mt.svg $(INSTALL_DIR)/share/icons/hicolor/scalable/apps/com.gitlab.miridyan.Mt.svg
	$(ROOT_PREFIX) install -m 644 data/icons/com.gitlab.miridyan.Mt.Devel.svg $(INSTALL_DIR)/share/icons/hicolor/scalable/apps/com.gitlab.miridyan.Mt.Devel.svg

# Uninstall the application
uninstall:
	$(ROOT_PREFIX) rm $(INSTALL_DIR)/bin/Mt
	$(ROOT_PREFIX) rm $(INSTALL_DIR)/share/Mt/profiles/Default.ron
	$(ROOT_PREFIX) rm $(INSTALL_DIR)/share/applications/com.gitlab.miridyan.Mt.desktop
	$(ROOT_PREFIX) rm $(INSTALL_DIR)/share/icons/hicolor/scalable/apps/com.gitlab.miridyan.Mt.svg
	$(ROOT_PREFIX) rm $(INSTALL_DIR)/share/icons/hicolor/scalable/apps/com.gitlab.miridyan.Mt.Devel.svg

# Clean directory
clean:
	cargo clean
