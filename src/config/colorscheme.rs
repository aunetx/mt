use crate::util;
use gdk::RGBA;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ColorScheme {
    pub bg: u32,
    pub fg: u32,
    pub window: u32,
    pub cursor: u32,
    pub colors: Vec<u32>,
}

impl ColorScheme {
    pub fn into_gdk(&self) -> (RGBA, RGBA, RGBA, Vec<RGBA>) {
        let bg = util::u32_to_rgba(self.bg);
        let fg = util::u32_to_rgba(self.fg);
        let cursor = util::u32_to_rgba(self.cursor);
        let colorscheme = self
            .colors
            .iter()
            .map(|&c| util::u32_to_rgba(c))
            .collect::<Vec<RGBA>>();

        (bg, fg, cursor, colorscheme)
    }
}

#[rustfmt::skip]
impl Default for ColorScheme {
    fn default() -> Self {
        Self {
            bg: 0x1d1f21ff,
            fg: 0xc5c8c6ff,
            window: 0x111213ff,
            cursor: 0xc5c8c6ff,
            colors: vec![
                //black     red         green       yellow      blue        purlple     aqua        white
                0x282a2eff, 0xa54242ff, 0x8c9440ff, 0xde935fff, 0x5f819dff, 0x85678fff, 0x5e8d87ff, 0x707880ff, 
                0x373b41ff, 0xcc6666ff, 0xb5bd68ff, 0xf0c674ff, 0x81a2beff, 0xb294bbff, 0x8abeb7ff, 0xc5c8c6ff,
            ],
        }
    }
}
