// use crate::{error::*, util};

use serde::{Deserialize, Serialize};

// For normal keyboard
#[derive(Copy, Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum Layout {
    Standard,
    Compact,
    Unified,
}

#[derive(Copy, Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum SessionBehavior {
    NewTab,
    NewWindow,
}

#[derive(Copy, Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum ScrollbarVisibility {
    Hidden,
    Visible,
    Overlay,
}

#[derive(Copy, Clone, Debug, PartialEq, Deserialize, Serialize)]
pub enum Theme {
    System,
    Light,
    Dark,
    FollowProfile,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum CursorShape {
    Block,
    IBeam,
    Underline,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum CursorBlinkMode {
    System,
    On,
    Off,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum EraseBinding {
    Auto,
    AsciiBackspace,
    AsciiDelete,
    DeleteSequence,
    Tty,
}

#[derive(Copy, Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum FontWeight {
    Thin,
    Ultralight,
    Light,
    Semilight,
    Book,
    Normal,
    Medium,
    Semibold,
    Bold,
    Ultrabold,
    Heavy,
    Ultraheavy,
}

impl Default for Layout {
    fn default() -> Self {
        Self::Unified
    }
}

impl Default for SessionBehavior {
    fn default() -> Self {
        Self::NewTab
    }
}

impl Default for ScrollbarVisibility {
    fn default() -> Self {
        Self::Hidden
    }
}

impl Default for Theme {
    fn default() -> Self {
        Self::System
    }
}

impl Default for CursorShape {
    fn default() -> Self {
        CursorShape::Block
    }
}

impl Default for CursorBlinkMode {
    fn default() -> Self {
        CursorBlinkMode::System
    }
}

impl Default for EraseBinding {
    fn default() -> Self {
        EraseBinding::Auto
    }
}

impl Default for FontWeight {
    fn default() -> Self {
        FontWeight::Normal
    }
}

/// This is gross, I know, but I can't directly deserialized a struct
/// I don't own.
impl From<CursorShape> for vte::CursorShape {
    fn from(s: CursorShape) -> Self {
        match s {
            CursorShape::Block => Self::Block,
            CursorShape::IBeam => Self::Ibeam,
            CursorShape::Underline => Self::Underline,
        }
    }
}

impl From<CursorBlinkMode> for vte::CursorBlinkMode {
    fn from(b: CursorBlinkMode) -> Self {
        match b {
            CursorBlinkMode::System => Self::System,
            CursorBlinkMode::On => Self::On,
            CursorBlinkMode::Off => Self::Off,
        }
    }
}

impl From<EraseBinding> for vte::EraseBinding {
    fn from(e: EraseBinding) -> Self {
        match e {
            EraseBinding::Auto => Self::Auto,
            EraseBinding::AsciiBackspace => Self::AsciiBackspace,
            EraseBinding::AsciiDelete => Self::AsciiDelete,
            EraseBinding::DeleteSequence => Self::DeleteSequence,
            EraseBinding::Tty => Self::Tty,
        }
    }
}

impl From<FontWeight> for pango::Weight {
    fn from(f: FontWeight) -> Self {
        match f {
            FontWeight::Thin => Self::Thin,
            FontWeight::Ultralight => Self::Ultralight,
            FontWeight::Light => Self::Light,
            FontWeight::Semilight => Self::Semilight,
            FontWeight::Book => Self::Book,
            FontWeight::Normal => Self::Normal,
            FontWeight::Medium => Self::Medium,
            FontWeight::Semibold => Self::Semibold,
            FontWeight::Bold => Self::Bold,
            FontWeight::Ultrabold => Self::Ultrabold,
            FontWeight::Heavy => Self::Heavy,
            FontWeight::Ultraheavy => Self::Ultraheavy,
        }
    }
}
