use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use Action::*;

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub enum Action {
    NewTab,
    CloseActiveTab,
    Copy,
    Paste,
    Quit,
    SwitchToTab(u8),
    UpdateStylesheet,
    About,
    SwitchPreviousTab,
    SwitchNextTab,
    CloseTabsToLeft,
    CloseTabsToRight,
    SetFullscreen,
    SetWindowed,
    SwitchFullscreen,
}

impl Action {
    fn to_string(&self) -> String {
        match self {
            NewTab => "win.new-tab".to_string(),
            CloseActiveTab => "win.close-active-tab".to_string(),
            Copy => "win.clipboard-copy".to_string(),
            Paste => "win.clipboard-paste".to_string(),
            Quit => "app.quit".to_string(),
            SwitchToTab(i) => format!("win.select-tab-{}", i),
            UpdateStylesheet => "app.update-stylesheet".to_string(),
            About => "app.about".to_string(),
            SwitchPreviousTab => "win.select-tab-left".to_string(),
            SwitchNextTab => "win.select-tab-right".to_string(),
            CloseTabsToLeft => "win.close-tabs-left".to_string(),
            CloseTabsToRight => "win.close-tabs-right".to_string(),
            SetFullscreen => "win.set-fullscreen".to_string(),
            SetWindowed => "win.set-windowed".to_string(),
            SwitchFullscreen => "win.switch-fullscreen".to_string(),
        }
    }
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct KeyBinding {
    action: Action,
    accel: String,
}

impl KeyBinding {
    fn default(action: Action) -> Self {
        match action {
            NewTab => Self {
                action,
                accel: "<primary><shift>t".to_string(),
            },
            CloseActiveTab => Self {
                action,
                accel: "<primary><shift>w".to_string(),
            },
            Copy => Self {
                action,
                accel: "<primary><shift>c".to_string(),
            },
            Paste => Self {
                action,
                accel: "<primary><shift>v".to_string(),
            },
            Quit => Self {
                action,
                accel: "<primary><shift>q".to_string(),
            },
            SwitchToTab(i) => Self {
                action,
                accel: format!("<alt>{}", i % 10),
            },
            UpdateStylesheet => Self {
                action,
                accel: "".to_string(),
            },
            About => Self {
                action,
                accel: "".to_string(),
            },
            SwitchPreviousTab => Self {
                action,
                accel: "<alt>Left".to_string(),
            },
            SwitchNextTab => Self {
                action,
                accel: "<alt>Right".to_string(),
            },
            CloseTabsToLeft => Self {
                action,
                accel: "".to_string(),
            },
            CloseTabsToRight => Self {
                action,
                accel: "".to_string(),
            },
            SetFullscreen => Self {
                action,
                accel: "<alt>F11".to_string(),
            },
            SetWindowed => Self {
                action,
                accel: "<alt><shift>F11".to_string(),
            },
            SwitchFullscreen => Self {
                action,
                //accel: "<alt>F11".to_string(),
                accel: "".to_string(),
            },
        }
    }
}

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ActionsKeymap(Vec<KeyBinding>);

impl ActionsKeymap {
    pub fn to_hashmap(self) -> HashMap<String, Vec<String>> {
        let mut res: HashMap<String, Vec<String>> = HashMap::new();
        for keybinding in self.0 {
            if keybinding.accel != "".to_string() {
                match res.get_mut(&keybinding.action.to_string()) {
                    Some(accels) => {
                        if !accels.contains(&keybinding.accel) {
                            accels.push(keybinding.accel);
                        }
                    }
                    None => {
                        res.insert(keybinding.action.to_string(), vec![keybinding.accel]);
                    }
                }
            }
        }
        res
    }

    pub fn add_defaults(&mut self) {
        self.0.extend_from_slice(&[
            KeyBinding::default(NewTab),
            KeyBinding::default(CloseActiveTab),
            KeyBinding::default(Copy),
            KeyBinding::default(Paste),
            KeyBinding::default(Quit),
            KeyBinding::default(UpdateStylesheet),
            KeyBinding::default(About),
            KeyBinding::default(SwitchPreviousTab),
            KeyBinding::default(SwitchNextTab),
            KeyBinding::default(CloseTabsToLeft),
            KeyBinding::default(CloseTabsToRight),
            KeyBinding::default(SetFullscreen),
            KeyBinding::default(SetWindowed),
            KeyBinding::default(SwitchFullscreen),
        ]);
        for i in 1..11 {
            self.0.push(KeyBinding::default(SwitchToTab(i)));
        }
    }
}

impl Default for ActionsKeymap {
    fn default() -> Self {
        let mut keymap = Vec::from([
            KeyBinding::default(NewTab),
            KeyBinding::default(CloseActiveTab),
            KeyBinding::default(Copy),
            KeyBinding::default(Paste),
            KeyBinding::default(Quit),
            KeyBinding::default(UpdateStylesheet),
            KeyBinding::default(About),
            KeyBinding::default(SwitchPreviousTab),
            KeyBinding::default(SwitchNextTab),
            KeyBinding::default(CloseTabsToLeft),
            KeyBinding::default(CloseTabsToRight),
            KeyBinding::default(SetFullscreen),
            KeyBinding::default(SetWindowed),
            KeyBinding::default(SwitchFullscreen),
        ]);
        for i in 1..11 {
            keymap.push(KeyBinding::default(SwitchToTab(i)));
        }

        Self(keymap)
    }
}
