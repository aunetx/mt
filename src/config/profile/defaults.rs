use super::Font;
use crate::{config::enums::*, util};

use once_cell::sync::OnceCell;
use serde::{de, Deserialize, Deserializer, Serialize};
use std::{env, ops::Deref, result};

pub(super) static PROFILE_DEFAULTS: OnceCell<Defaults> = OnceCell::new();

/// Subset of profile options, forces you to define at least one profile
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Defaults {
    #[serde(default = "Defaults::default_scrollback")]
    pub scrollback: i64,

    #[serde(default = "Defaults::default_dimensions")]
    pub dimensions: (u32, u32),

    #[serde(default = "Defaults::default_cell_spacing")]
    pub cell_spacing: (f32, f32),

    #[serde(default)]
    pub cursor_style: CursorShape,

    #[serde(default)]
    pub cursor_blink_mode: CursorBlinkMode,

    #[serde(default)]
    pub erase_binding: EraseBinding,

    #[serde(default)]
    pub audible_bell: bool,

    #[serde(default = "Defaults::default_colorscheme")]
    pub colorscheme: String,

    #[serde(default)]
    pub font: Font,

    #[serde(
        default = "Defaults::default_cwd",
        deserialize_with = "Defaults::deserialize_cwd"
    )]
    pub cwd: String,

    #[serde(
        default = "Defaults::default_cmd",
        deserialize_with = "Defaults::deserialize_cmd"
    )]
    pub cmd: String,
}

impl Defaults {
    /// When deserializing the string for the working directory, parse any environment
    /// variables and verify that it is a valid path.
    fn deserialize_cwd<'de, D>(deserializer: D) -> result::Result<String, D::Error>
    where
        D: Deserializer<'de>,
    {
        let cwd = String::deserialize(deserializer)?;
        util::parse_path(cwd)
            .and_then(|pathbuf| Some(pathbuf.to_string_lossy().deref().to_owned()))
            .ok_or(de::Error::custom("CWD not found!"))
    }

    fn deserialize_cmd<'de, D>(deserializer: D) -> result::Result<String, D::Error>
    where
        D: Deserializer<'de>,
    {
        let cmd = String::deserialize(deserializer)?;
        util::parse_path(cmd)
            .and_then(|pathbuf| Some(pathbuf.to_string_lossy().deref().to_owned()))
            .ok_or(de::Error::custom("CMD not found!"))
    }

    #[doc(hidden)]
    fn default_scrollback() -> i64 {
        1_000
    }

    #[doc(hidden)]
    fn default_cmd() -> String {
        env::var("SHELL").unwrap()
    }

    #[doc(hidden)]
    fn default_cwd() -> String {
        env::var("HOME").unwrap()
    }

    #[doc(hidden)]
    fn default_colorscheme() -> String {
        String::from("Base16 Dark Default")
    }

    #[doc(hidden)]
    fn default_dimensions() -> (u32, u32) {
        if let Some(default) = PROFILE_DEFAULTS.get() {
            return default.dimensions;
        }
        (80, 25)
    }

    #[doc(hidden)]
    fn default_cell_spacing() -> (f32, f32) {
        (1.0, 1.0)
    }
}

impl Default for Defaults {
    fn default() -> Self {
        Self {
            scrollback: Defaults::default_scrollback(),
            dimensions: Defaults::default_dimensions(),
            cell_spacing: Defaults::default_cell_spacing(),
            cursor_style: Default::default(),
            cursor_blink_mode: Default::default(),
            erase_binding: Default::default(),
            audible_bell: Default::default(),
            colorscheme: Defaults::default_colorscheme(),
            font: Default::default(),
            cwd: Defaults::default_cwd(),
            cmd: Defaults::default_cmd(),
        }
    }
}
