#![deny(rust_2018_idioms)]
//#![cfg_attr(not(debug_assertions), deny(warnings))]

mod app;
mod config;
mod error;
mod message;
mod traits;
mod util;
mod vte;
mod widget;
mod window;

use crate::{app::MtApplication, error::*};

fn main() -> Result<()> {
    #[cfg(debug_assertions)]
    std::env::set_var("RUST_LOG", "trace");

    util::init()?;

    MtApplication::run();
    Ok(())
}
