use crate::{
    config::{ColorScheme, Config},
    error::*,
    traits::*,
};

use gdk::{Screen, RGBA};
use gio::{prelude::*, resources_lookup_data, Resource, ResourceLookupFlags};
use glib::{Bytes, Variant, VariantTy};
use gtk::{prelude::*, CssProvider, StyleContext};
use log::{error, warn};
use regex::{Captures, Regex};
use std::{
    env, fs,
    ops::Deref,
    path::{Path, PathBuf},
    str,
};
use xdg::BaseDirectories;

const R_MASK: u32 = 0xff000000;
const G_MASK: u32 = 0x00ff0000;
const B_MASK: u32 = 0x0000ff00;
const A_MASK: u32 = 0x000000ff;

pub const ENV_REGEX: &'static str = r"\$[A-Za-z0-9_]+";
// pub type ProfileCache = Rc<RefCell<HashMap<String, Profile>>>;

// Thank you gnome podcasts
pub fn action<T, F>(thing: &T, name: &str, action: F, param_type: Option<&VariantTy>)
where
    T: ActionMapExt,
    F: Fn(&gio::SimpleAction, Option<&Variant>) + 'static,
{
    let act = gio::SimpleAction::new(name, param_type);

    act.connect_activate(action);
    thing.add_action(&act);
}

// pub fn close_tab(app: &Application, notebook: &Notebook, page_num: Option<u32>) {
//     if let Some(page_num) = page_num {
//         let length = notebook.get_n_pages();
//         if page_num < length {
//             if length == 1 {
//                 debug!("Only 1 page open: quitting app");
//                 app.quit();
//             } else {
//                 notebook.remove_page(Some(page_num));
//                 debug!("Closing page {}", page_num);
//             }
//         } else {
//             warn!(
//                 "Requested tab closure index is greater than size of notebook {} > {}",
//                 page_num, length
//             );
//         }
//     }
// }

// pub fn update_titles(
//     window: &MtApplicationWindow,
//     notebook: &Notebook,
//     header: &MtTabHeader,
//     terminal: &MtTerminal,
//     vte: &Terminal,
// ) {
//     match vte.get_window_title() {
//         Some(s) => {
//             header.set_title(s.as_str());
//             if notebook.page_num(terminal) == notebook.get_current_page() {
//                 window.set_titles(s.as_str());
//             }
//         }
//         None => {
//             header.set_title("");
//         }
//     };
//     debug!("Updating tab title to {:?}", header.get_title());
// }

/// This embeds the `gresources` file located at `/gresource/com.gitlab.miridyan.Mt.gresource`
/// into the binary at compile-time and registers the resources. All resources can be
/// accessed by calling the `gio::resources_lookup_data` function.
pub fn load_gresources() -> Result<()> {
    let res = Resource::from_data(&Bytes::from_static(include_bytes!(
        "../target/com.gitlab.miridyan.Mt.gresource"
    )))?;

    gio::resources_register(&res);

    Ok(())
}

/// This accesses the `gtk.css` file in `resources` and applies it to the panel.
/// It creates a `CssProvider`, loads the stylesheet into it, and applies it to
/// the default `gdk::Screen`.
pub fn load_stylesheets(titlebar: &str, tab: &str) -> Result<()> {
    let provider = CssProvider::new();
    let screen = Screen::get_default().unwrap();
    let base_res = "/com/gitlab/miridyan/Mt/theme/gtk.css";
    let stylesheet = str::from_utf8(&*resources_lookup_data(
        base_res,
        ResourceLookupFlags::NONE,
    )?)?
    .to_string();

    let stylesheet = stylesheet
        .replace("{{titlebar}}", titlebar)
        .replace("{{tab-active}}", tab);

    provider.load_from_data(stylesheet.as_bytes())?;
    StyleContext::add_provider_for_screen(
        &screen,
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );

    Ok(())
}

/// I can't implement `Into<T>` or `From<T>` for `u32` or `gdk::RGBA` so I guess this is
/// my only choice
pub fn u32_to_rgba(c: u32) -> RGBA {
    let r = (c & R_MASK) >> 24;
    let g = (c & G_MASK) >> 16;
    let b = (c & B_MASK) >> 8;
    let a = c & A_MASK;

    RGBA {
        red: r as f64 / 255.0,
        green: g as f64 / 255.0,
        blue: b as f64 / 255.0,
        alpha: a as f64 / 255.0,
    }
}

/// Update this later to pull all profiles from the folder and to put them in the
/// profile cache. For now this will just hand the default profile. Shouldn't be too
/// difficult to add the needed functionality.
pub fn initialize_environment() -> (Config, BaseDirectories) {
    // let profile_cache = Rc::new(RefCell::new(HashMap::new()));
    let xdg_directories = BaseDirectories::with_prefix("Mt").unwrap();
    let config = xdg_directories.load_config_or_default::<Config>("config.ron");

    // profile_cache
    //     .borrow_mut()
    //     .insert(profile.name.clone(), profile);
    (config, xdg_directories)
}

pub fn update_stylesheet(colorscheme: &ColorScheme) {
    // // This approach works fine, but doesn't allow for as refined logging
    // // so IDK if I want to actually use this, but I will keep it here in
    // // the comments for now. I'm not sure if I actually want this function
    // // to eventually return a `Result<()>`, but for now I'm okay with this
    // // function basically "fail silently".
    // let pc_borrowed = pc.borrow();

    // profile_name.and_then(|profile_name| profile_name.get_str())
    //     .and_then(|profile_name| pc_borrowed.get(profile_name))
    //     .and_then(|profile| {
    //         let c = &profile.colorscheme;
    //         let tab = format!("{:?}", c.bg >> 8).to_string();
    //         let titlebar = format!("{:?}", c.window >> 8).to_string();

    //         load_stylesheets(&titlebar, &tab).or_else(|e| error!("{}", e));

    //         Some(())
    //     });
    let tab = format!("{:06x}", colorscheme.bg >> 8).to_string();
    let titlebar = format!("{:06x}", colorscheme.window >> 8).to_string();
    if let Err(e) = load_stylesheets(&titlebar, &tab) {
        error!("{}", e);
    }

    // match profile_name {
    //     Some(profile_name) => {
    //         let profile_name = profile_name.get_str().unwrap();

    //         match &pc.borrow().get(profile_name) {
    //             Some(profile) => {
    //                 let c = &profile.colorscheme;
    //                 let tab = format!("{:06x}", c.bg >> 8).to_string();
    //                 let titlebar = format!("{:06x}", c.window >> 8).to_string();
    //                 if let Err(e) = load_stylesheets(&titlebar, &tab) {
    //                     error!("{}", e);
    //                 }
    //             }
    //             None => error!("Stylesheet \"{}\" not found", profile_name),
    //         }
    //     }
    //     None => error!("No profile name provided"),
    // }
}

/// Attempt to identify, resolve, and replace all environment variables. After a run through
/// the string, check to see if any environment variables were not replaced. If all were
/// replaced, execute `f(parsed_str)`, if one or more was not, execute `g(parsed_str)`.
pub fn parse_env_regex_on_fail<T, F, G>(mut path: String, f: F, g: G) -> Option<T>
where
    F: Fn(String) -> Option<T>,
    G: Fn(String) -> Option<T>,
{
    let reg = Regex::new(ENV_REGEX).expect("Failed to parse ENV_REGEX");

    if reg.is_match(&path) {
        path = reg
            .replace_all(&path, |caps: &Captures<'_>| match env::var(&caps[0][1..]) {
                Ok(val) => val,
                Err(e) => {
                    let unchanged = caps[0].to_string();
                    log::warn!("Could not find environment variable {} : {}", unchanged, e);
                    unchanged
                }
            })
            .deref()
            .to_string();
    }

    if !reg.is_match(&path) {
        f(path)
    } else {
        g(path)
    }
}

/// Like `parse_env_regex_on_fail`, but in the event that any environment variable cannot be
/// resolved, return `None`.
pub fn parse_env_regex<T, F>(path: String, f: F) -> Option<T>
where
    F: Fn(String) -> Option<T>,
{
    parse_env_regex_on_fail(path, f, |_| None)
}

/// Given a string that represents a path that may potentially contain environment variables,
/// parse out and replace all environment variables. Once finished, attempt to canonicalize
/// the path with `std::fs::canonicalize`. If either the replacement of environment variables
/// or path canonicalization fails, return `None`.
pub fn parse_path(path: String) -> Option<PathBuf> {
    parse_env_regex(path, |r_path| {
        fs::canonicalize(&Path::new(&r_path))
            .or_else(|err| {
                warn!("Failed to canonicalize path {:?}", err);
                Err(err)
            })
            .ok()
    })
}

/// Break a command into two parts, the command and the parameters. For all parts of the command,
/// parse out and replace any and all environment variables. If that variable cannot be resolved,
/// just don't replace it and pass the string on. For the command, check if the command exists
/// in the filesystem. If not, search the path for the command and if found prepend that
/// respective portion of the $PATH to the command.
pub fn parse_cmd(cmd: String) -> Option<String> {
    let mut split = cmd
        .splitn(2, " ")
        .map(String::from)
        .collect::<Vec<String>>();
    let cmd = parse_env_regex(split[0].clone(), |p_cmd| {
        fs::metadata(&p_cmd)
            .ok()
            // TODO:
            // use `bool::then()` here when it's stabilized, can make this cleaner and
            // remove all if statements from closures
            .and_then(|metadata| Some(metadata.is_file()))
            .or_else(|| Some(false))
            .and_then(|is_file| {
                if !is_file {
                    env::var_os("PATH").and_then(|paths| {
                        env::split_paths(&paths)
                            .filter_map(|dir| {
                                let full = dir.join(p_cmd.deref().to_owned());
                                // will be able to use `bool::then()` here too
                                // no more ifs!
                                if full.is_file() {
                                    log::info!("Found command file at {:?}", full);
                                    full.to_str().map(String::from)
                                } else {
                                    None
                                }
                            })
                            .next()
                    })
                } else {
                    Some(p_cmd)
                }
            })
    });

    if let Some(new_cmd) = cmd {
        split[0] = new_cmd;
    } else {
        return None;
    }

    if split.len() == 2 {
        // If the environment variable fails to resolve, just return the string anyway
        // I'll let the software you're calling deal with it. Who knows, maybe it can export
        // some value to environment variable? Anyway, it's not my problem, so I'll just let
        // it fall through.
        let f = |p_param| Some(p_param);
        let params = parse_env_regex_on_fail(split[1].clone(), f, f);

        if let Some(new_params) = params {
            log::info!("Command parameters {}", new_params);
            split[1] = new_params;
        } else {
            return None;
        }
    }

    Some(split.join(" "))
}

pub fn init() -> Result<()> {
    env_logger::init();
    gtk::init()?;
    libhandy::init();
    load_gresources()?;

    Ok(())
}
