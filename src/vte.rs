use gdk::RGBA;
use gtk::prelude::*;
// use vte_sys::vte_terminal_set_colors;
use gdk_sys::GdkRGBA;
use glib::translate::ToGlibPtr;
use pango::FontDescription;
use vte::{CursorBlinkMode, CursorShape, EraseBinding, Terminal, TerminalExt};
use vte_sys::vte_terminal_set_colors;

/// Structure to act as builder for terminal widget. This is a basic wrapper around
/// the `vte::Terminal` object.
pub struct VteBuilder {
    t: Terminal,
}

/// I don't normally like allowing dead code in my projects, but I'll make an exception here because
/// this is basically a comprensive builder wrapper around the `vte::Terminal`. I want it to fully
/// encompass all possible settings for `vte::Terminal` even though I may not use most of them.
#[allow(dead_code)]
impl VteBuilder {
    /// Create new builder
    pub fn new() -> Self {
        Self { t: Terminal::new() }
    }

    /// Set the front properties of the `vte::Terminal` with a `pango::FontDescription`
    pub fn font(self, font: FontDescription) -> Self {
        self.t.set_font(Some(&font));
        self
    }

    /// Set the size of the terminal window in terms of `rows` and `columns`
    pub fn size(self, size: (u32, u32)) -> Self {
        let (cols, rows) = size;
        self.t.set_size(cols as i64, rows as i64);
        self
    }

    /// Allow bold fonts to display in the terminal
    pub fn allow_bold(self, bold: bool) -> Self {
        self.t.set_allow_bold(bold);
        self
    }

    /// Make the `vte::Terminal` widget expand to fit horizontally
    pub fn hexpand(self, h: bool) -> Self {
        self.t.set_hexpand(h);
        self
    }

    /// Make the `vte::Terminal` widget expand to fit vertically
    pub fn vexpand(self, v: bool) -> Self {
        self.t.set_vexpand(v);
        self
    }

    pub fn audible_bell(self, b: bool) -> Self {
        self.t.set_audible_bell(b);
        self
    }

    /// Scroll on keystroke
    pub fn scroll_on_keystroke(self, s: bool) -> Self {
        self.t.set_scroll_on_keystroke(s);
        self
    }

    /// Scroll on command output
    pub fn scroll_on_output(self, s: bool) -> Self {
        self.t.set_scroll_on_output(s);
        self
    }

    /// Set number of scrollback lines
    pub fn scrollback_lines(self, num: i64) -> Self {
        self.t.set_scrollback_lines(num);
        self
    }

    /// When resizing, tell the `vte::Terminal` to rewrap text
    pub fn rewrap_on_resize(self, b: bool) -> Self {
        self.t.set_rewrap_on_resize(b);
        self
    }

    /// The blink mode of the cursor
    pub fn cursor_blink_mode(self, mode: CursorBlinkMode) -> Self {
        self.t.set_cursor_blink_mode(mode);
        self
    }

    /// Set the cursor to block or i-beam
    pub fn cursor_shape(self, shape: CursorShape) -> Self {
        self.t.set_cursor_shape(shape);
        self
    }

    pub fn backspace_binding(self, b: EraseBinding) -> Self {
        self.t.set_backspace_binding(b);
        self
    }

    pub fn colorscheme(self, bg: &RGBA, fg: &RGBA, colorscheme: &Vec<RGBA>) -> Self {
        // let c = colorscheme.iter().map(|c| c.to_glib_none().0).collect::<Vec<GdkRGBA>>();
        let mut c: Vec<GdkRGBA> = Vec::with_capacity(colorscheme.len());

        unsafe {
            for i in 0..colorscheme.len() {
                c.push(*colorscheme[i].to_glib_none().0);
            }

            vte_terminal_set_colors(
                self.t.to_glib_none().0,
                fg.to_glib_none().0,
                bg.to_glib_none().0,
                c.as_ptr(),
                colorscheme.len(),
            );
            self
        }
    }

    /// Convert a `VteBuilder` to a `vte::Terminal`
    pub fn build(self) -> Terminal {
        self.t
    }
}

/// Because all GTK setter methods take `&self`, we can implement `Deref` on `VteBuilder`
/// and use `self.set_some_gtk_prop()` instead of `self.t.set_some_gtk_prop()`. It's not
/// really necessary, but it makes things look nicer.
impl std::ops::Deref for VteBuilder {
    type Target = Terminal;

    fn deref(&self) -> &Self::Target {
        &self.t
    }
}
