use crate::{
    app::TerminalParams,
    config::{Profile, ScrollbarVisibility},
    message::WindowCommand,
    vte::VteBuilder,
    widget::header::MtTabHeader,
    window::MtApplicationWindow,
};
use gio::Cancellable;
use glib::{
    clone,
    prelude::*,
    subclass,
    subclass::prelude::*,
    translate::{FromGlibPtrFull, ToGlib, ToGlibPtr},
    Object, Sender, SpawnFlags,
};
use gtk::{
    prelude::*, subclass::prelude::*, Adjustment, Box, /*Builder, Button, Menu,*/ Orientation,
    PolicyType, ScrolledWindow, ScrolledWindowBuilder, ShadowType,
};
use once_cell::unsync::OnceCell;
use std::{
    cell::Cell,
    ops::Deref,
    path::{Path, PathBuf},
    rc::Rc,
};
use vte::{PtyFlags, Terminal, TerminalExt};

#[derive(Clone, Debug)]
pub struct MtTerminalPriv {
    widgets: OnceCell<Widgets>,
    profile: OnceCell<Profile>,
    sender: OnceCell<Sender<WindowCommand>>,
    pid: Rc<Cell<glib::Pid>>,
}

#[derive(Clone, Debug)]
struct Widgets {
    terminal: Terminal,
    scrolled_window: ScrolledWindow,
}

impl ObjectSubclass for MtTerminalPriv {
    const NAME: &'static str = "MtTerminal";

    type ParentType = Box;
    type Class = subclass::simple::ClassStruct<Self>;
    type Instance = subclass::simple::InstanceStruct<Self>;
    glib::glib_object_subclass!();

    fn new() -> Self {
        Self {
            widgets: OnceCell::new(),
            profile: OnceCell::new(),
            sender: OnceCell::new(),
            pid: Rc::new(Cell::new(glib::Pid(-1i32))),
        }
    }
}

impl ObjectImpl for MtTerminalPriv {
    glib::glib_object_impl!();
}

impl MtTerminalPriv {
    fn set_widgets(&self, widgets: Widgets) {
        self.widgets.set(widgets).expect("Failed to set widgets");
    }

    fn set_profile(&self, profile: Profile) {
        self.profile.set(profile).expect("Failed to set profile");
    }

    fn set_sender(&self, sender: Sender<WindowCommand>) {
        self.sender.set(sender).expect("Failed to set sender");
    }

    #[cfg(debug_assertions)]
    fn profile(&self) -> Option<&Profile> {
        self.profile.get()
    }

    fn terminal(&self) -> Option<&Terminal> {
        if let Some(widgets) = self.widgets.get() {
            return Some(&widgets.terminal);
        }

        None
    }
}

impl WidgetImpl for MtTerminalPriv {}
impl ContainerImpl for MtTerminalPriv {}
impl BoxImpl for MtTerminalPriv {}

glib::glib_wrapper! {
    /// This struct corresponds to a single VTE Terminal and its corresponding profile. Because multiple
    /// tabs can be open with different profiles, it makes sense to store it with the profile. This will
    /// end up redundant if all the terminals end up with same profile, so eventually this should probably
    /// be refactored to store a UUID for the profile or something and then reference a profile HashMap
    /// in the main Model of the application.
    pub struct MtTerminal(
        Object<
            subclass::simple::InstanceStruct<MtTerminalPriv>,
            subclass::simple::ClassStruct<MtTerminalPriv>,
            MtTerminalClass
        >
    ) @extends gtk::Widget, gtk::Container, gtk::Box;

    match fn {
        get_type => || MtTerminalPriv::get_type().to_glib(),
    }
}

impl MtTerminal {
    pub fn new(
        _w: &MtApplicationWindow,
        t: TerminalParams,
        cwd: Option<PathBuf>,
        cmd: Option<Vec<PathBuf>>,
        sender: Sender<WindowCommand>,
    ) -> Self {
        let box_ = Object::new(
            Self::static_type(),
            &[
                ("hexpand", &true),
                ("vexpand", &true),
                ("orientation", &Orientation::Horizontal),
                ("spacing", &0),
            ],
        )
        .expect("Failed to create MtTerminal")
        .downcast::<MtTerminal>()
        .expect("Created MtTerminal is of wrong type");

        let priv_ = MtTerminalPriv::from_instance(&box_);
        let TerminalParams(p, c, s) = t;
        let (font, allow_bold) = p.font();
        let (bg, fg, _, colorscheme) = c.into_gdk();
        let cmd = if let Some(cmd) = cmd { cmd } else { p.cmd() };

        log::debug!("CMD {:?}", cmd);

        let cwd = cwd
            .and_then(|path| Some(path.to_string_lossy().deref().to_owned()))
            .unwrap_or_else(|| p.cwd.to_owned());

        let terminal = VteBuilder::new()
            .font(font)
            .hexpand(true)
            .vexpand(true)
            .size(p.dimensions)
            .audible_bell(p.audible_bell)
            .allow_bold(allow_bold)
            .colorscheme(&bg, &fg, &colorscheme)
            .scrollback_lines(p.scrollback)
            .cursor_shape(p.cursor_style.into())
            .cursor_blink_mode(p.cursor_blink_mode.into())
            .backspace_binding(p.erase_binding.into())
            .build();

        // Need to add error checking system here
        let pid = terminal
            .spawn_sync::<Cancellable>(
                PtyFlags::DEFAULT,
                Some(&cwd),
                &cmd.iter().map(PathBuf::as_path).collect::<Vec<&Path>>(),
                &[],
                SpawnFlags::DEFAULT | SpawnFlags::SEARCH_PATH_FROM_ENVP,
                Some(&mut || {}),
                None,
            )
            .unwrap();

        // Look, I know this is bad, and that I probably shouldn't do this, but right now it makes
        // scrollbars work, so it's what I'm sticking with.
        let adjustment: Adjustment = unsafe {
            let term_ref: &Terminal = terminal.as_ref();
            let scrollable_ptr: *mut vte_sys::VteTerminal = term_ref.to_glib_none().0;
            let adjustment = gtk_sys::gtk_scrollable_get_vadjustment(
                scrollable_ptr as *mut gtk_sys::GtkScrollable,
            );

            glib::translate::from_glib_none(adjustment)
        };

        let scroll_policy = if s != ScrollbarVisibility::Hidden {
            PolicyType::Automatic
        } else {
            PolicyType::Never
        };

        let overlay_scrolling = s == ScrollbarVisibility::Overlay;
        // let menu = {
        //     let menu_builder = Builder::from_resource("/com/gitlab/miridyan/Mt/ui/context-menu.ui");
        //     let menu_model = menu_builder.get_object::<MenuModel>("context-menu").unwrap();
        //     let menu = Menu::from_model(&menu_model);
        //     menu.set_property_attach_widget(Some(w));
        //     menu
        // };

        terminal.set_visible(true);
        // terminal.connect_popup_menu(move |_| {
        //     menu.popup_at_pointer(None);

        //     true
        // });
        // terminal.connect_event(|t, event| {
        //     if event.get_button().is_some() && event.get_button().unwrap() == 3 {
        //         t.emit_popup_menu();

        //         Inhibit(true)
        //     } else {
        //         Inhibit(false)
        //     }
        // });

        let scrolled_window = ScrolledWindowBuilder::new()
            .hscrollbar_policy(PolicyType::Never)
            .vscrollbar_policy(scroll_policy)
            .propagate_natural_height(true)
            .propagate_natural_width(true)
            .shadow_type(ShadowType::None)
            .vadjustment(&adjustment)
            .overlay_scrolling(overlay_scrolling)
            .child(&terminal)
            .vexpand(true)
            .hexpand(true)
            .build();

        scrolled_window.set_visible(true);
        box_.add(&scrolled_window);
        let widgets = Widgets {
            terminal,
            scrolled_window,
        };
        priv_.set_sender(sender);
        priv_.set_widgets(widgets);
        priv_.set_profile(p.to_owned());

        (*priv_.pid).set(pid);

        box_
    }

    #[cfg(debug_assertions)]
    pub fn profile(&self) -> &Profile {
        let priv_ = MtTerminalPriv::from_instance(self);
        priv_
            .profile()
            .expect("Failed to get profile, should never occur")
    }

    pub fn focus(&self) {
        let priv_ = MtTerminalPriv::from_instance(self);
        if let Some(terminal) = priv_.terminal() {
            terminal.grab_focus();
        }
    }

    pub fn get_window_title(&self) -> Option<String> {
        let priv_ = MtTerminalPriv::from_instance(self);

        if let Some(terminal) = priv_.terminal() {
            if let Some(title) = terminal.get_window_title() {
                return Some(title.as_str().to_owned());
            }
        }

        None
    }

    pub fn copy(&self) {
        let priv_ = MtTerminalPriv::from_instance(self);
        if let Some(terminal) = priv_.terminal() {
            terminal.copy_clipboard();
        }
    }

    pub fn set_geometry_hints(&self, w: &gtk::Window) {
        let priv_ = MtTerminalPriv::from_instance(self);
        if let Some(terminal) = priv_.terminal() {
            unsafe {
                let terminal_ptr: *mut vte_sys::VteTerminal = terminal.to_glib_none().0;
                let window_ptr: *mut gtk_sys::GtkWindow = w.to_glib_none().0;

                vte_sys::vte_terminal_set_geometry_hints_for_window(terminal_ptr, window_ptr);
            }
        }
    }

    pub fn paste(&self) {
        let priv_ = MtTerminalPriv::from_instance(self);
        if let Some(terminal) = priv_.terminal() {
            terminal.paste_clipboard();
        }
    }

    fn sender(&self) -> &Sender<WindowCommand> {
        let priv_ = MtTerminalPriv::from_instance(self);
        priv_.sender.get().unwrap()
    }

    pub fn assign_header(&self, header: &MtTabHeader, notebook: &gtk::Notebook) {
        let priv_ = MtTerminalPriv::from_instance(self);
        let terminal = priv_
            .terminal()
            .expect("Failed to get terminal, should never occur");

        header.connect_property_tab_close(
            clone!(@weak notebook, @weak self as terminal => move|_| {
                let _ = notebook
                    .page_num(&terminal)
                    .and_then(|page| {
                        terminal.sender().send(WindowCommand::CloseTab(page)).ok()
                    });
            }),
        );

        terminal.connect_child_exited(
            clone!(@weak notebook, @weak self as terminal => move|_, _| {
                let _ = notebook
                    .page_num(&terminal)
                    .and_then(|page| {
                        terminal.sender().send(WindowCommand::CloseTab(page)).ok()
                    });
            }),
        );

        terminal.connect_property_window_title_notify(
            clone!(@weak header, @weak notebook, @weak self as terminal => move |vte| {
                // let title = if let Some(title) = vte.get_window_title() {
                //     title.as_str().to_string()
                // } else {
                //     "".to_string()
                // };

                let page_num = notebook.page_num(&terminal);
                let title = vte.get_window_title()
                    .and_then(|s| Some(s.as_str().to_string()))
                    .or_else(|| Some("".to_string()))
                    .unwrap();

                // let _ = notebook.page_num(&terminal)
                //     .and_then(|page| {
                //         terminal.sender().send(WindowCommand::UpdateTabTitle { page, title }).ok()
                //     });

                if let Some(page) = page_num {
                    if let Err(e) = terminal.sender().send(WindowCommand::UpdateTabTitle { page, title: title.clone() }) {
                        log::error!("Failed to update tab title {:?}", e);
                    }
                }
                if page_num == notebook.get_current_page() && page_num.is_some() {
                    if let Err(e) = terminal.sender().send(WindowCommand::UpdateWindowTitle(title)) {
                        log::error!("Failed to update window title {:?}", e);
                    }
                }
                // match vte.get_window_title() {
                //     Some(s) => {
                //         header.set_title(s.as_str());
                //         let _ = terminal
                //             .sender()
                //             .send(
                //                 Message::UpdateTabTitle(
                //                     notebook.page_num(&terminal),
                //                     s.as_str().to_string()
                //                 )
                //             );
                //         if notebook.page_num(&terminal) == notebook.get_current_page() {
                //             let _ = terminal
                //                 .sender()
                //                 .send(Message::UpdateWindowTitle(String::from(s.as_str())))
                //                 .or_else::<std::sync::mpsc::SendError<Message>, _>(|e| {
                //                     log::error!("Failed to send `UpdateWindowTitle` {:?}", e);
                //                     Err(e)
                //                 });
                //         }
                //     },
                //     None => {
                //         header.set_title("");
                //         let _ = terminal
                //             .sender()
                //             .send(
                //                 Message::UpdateTabTitle(
                //                     notebook.page_num(&terminal),
                //                     String::from("")
                //                 )
                //             );
                //     },
                // };
            }),
        );
    }
}
